const std = @import("std");
const testing = std.testing;
const expect = std.testing.expect;
const Command = @import("./Command.zig");

const string = []const u8;

const ta = std.testing.allocator;

var commands = std.ArrayList(Command).init(ta);

pub fn commandDir(path: string) void {
    std.debug.print("the path: {s}", .{path});
}

const MyError = error{
    GENERAL,
};

pub fn addCommand(command: Command) void {
    try commands.append(command) || error.MyError;
}

test "test addCommand" {
    var cmd = Command{
        .command = 1,
        .description = 1,
    };
    try addCommand(cmd);
}
