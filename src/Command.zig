const std = @import("std");

const String = []const u8;
const Handler = *const fn () void;

/// Whether a param takes no value (a flag), one value, or can be specified multiple times.
pub const Values = enum {
    none,
    one,
    many,
};

/// Represents a parameter for the command line.
pub const Param = struct {
    // Name or form of the parameter
    name: String,
    /// The option description for help content
    description: String,
    /// Either `boolean`, `number`, or `string`
    type: String,
    /// Set a default value for the option
    default: String,
    alias: String,

    pub fn init() Param {
        return .{
            .name = undefined,
            .description = undefined,
            .type = undefined,
            .default = null,
            .alias = undefined,
        };
    }
};

pub const Command = struct {
    /// The allocator used for managing the buffer
    allocator: std.mem.Allocator,
    /// Command pattern
    command: String,
    /// Describe the command
    description: String,
    /// Arguments builder
    builder: String,
    /// Callback to execute when command is called
    handler: ?Handler,

    /// Creates a Command with an Allocator
    pub fn init(allocator: std.mem.Allocator) Command {
        return .{
            .allocator = allocator,
            .command = undefined,
            .description = undefined,
            .builder = undefined,
            .handler = undefined,
        };
    }
};

test "ctor command" {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    var cmd: Command = Command.init(allocator);
    cmd.command = "test";
    cmd.description = "This is command description";
    cmd.handler = struct {
        pub fn a() void {
            std.debug.print("Hello", .{});
        }
    }.a;
}
